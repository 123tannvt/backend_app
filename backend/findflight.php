<?php
include "../config/db_connect.php";
$idsanbaydi = $_POST['idsanbaydi'];
$idsanbayden = $_POST['idsanbayden'];
$date = $_POST['date'];

$arraysearch = array();


$query = "SELECT a.macb,date(a.giodi) as 'ngaydi', DATE_FORMAT(a.giodi, '%H:%i') as'giodi',date(a.gioden) as 'ngayden',DATE_FORMAT(a.gioden, '%H:%i') as'gioden',
a.duration,a.status,b.diachi as'diachidi',b.airport_no as'ma_sanbaydi',c.diachi as'diachiden', c.airport_no as 'ma_sanbayden'
FROM ((sanbay b join chuyenbay a on b.masb=a.sanbaydi_id) join sanbay c on c.masb=a.sanbayden_id ) WHERE  a.sanbaydi_id ='$idsanbaydi' and a.sanbayden_id = '$idsanbayden'  and a.status=''  and date(a.giodi) >= '$date'  and a.status='' and DATE_ADD(CURRENT_TIME, INTERVAL 3 HOUR) <= DATE_FORMAT(a.giodi, '%H:%i') order by rand()";

$data = mysqli_query($conn, $query);
$count = mysqli_num_rows($data);
if ($count >= 1) {
    while ($row = mysqli_fetch_assoc($data)) {
        array_push(
            $arraysearch,
            new searchflight(
                $row['macb'],
                $row['ngaydi'],
                $row['giodi'],
                $row['ngayden'],
                $row['gioden'],
                $row['diachidi'],
                $row['ma_sanbaydi'],
                $row['diachiden'],
                $row['ma_sanbayden'],
                $row['duration'],
                $row['status']
            )
        );
    }

    echo json_encode($arraysearch);
} else {
    echo json_encode("Error");
}
class searchflight
{
    function __construct($macb, $ngaydi, $giodi, $ngayden, $gioden, $diachidi, $ma_sanbaydi, $diachiden, $ma_sanbayden, $duration, $status)
    {
        $this->macb = $macb;
        $this->ngaydi = $ngaydi;
        $this->giodi = $giodi;
        $this->ngayden = $ngayden;
        $this->gioden = $gioden;
        $this->diachidi = $diachidi;
        $this->ma_sanbaydi = $ma_sanbaydi;
        $this->diachiden = $diachiden;
        $this->ma_sanbayden = $ma_sanbayden;
        $this->duration = $duration;
        $this->status = $status;
    }
}
?>