<?php
include('../config/db_connect.php');
// $masb = $_POST['masb'];
// $arraysb=

// $idflight = $_POST['macb'];
$arryflight = array();
$query1 = " SELECT a.macb,date(a.giodi) as 'ngaydi', DATE_FORMAT(a.giodi, '%H:%i') as'giodi',date(a.gioden) as 'ngayden',DATE_FORMAT(a.gioden, '%H:%i') as'gioden',
a.duration,a.status,b.tensb as'sanbaydi',b.airport_no as'ma_sanbaydi', c.airport_no as 'ma_sanbayden',c.tensb as'sanbayden'
 FROM ((sanbay b join chuyenbay a on b.masb=a.sanbaydi_id) join sanbay c on c.masb=a.sanbayden_id ) WHERE a.giodi >= NOW() AND a.status='' ";
// $query = "SELECT * FROM sanbay WHERE status=1";
$data = mysqli_query($conn, $query1);
$count = mysqli_num_rows($data);
if ($count >= 1) {
    while ($row = mysqli_fetch_assoc($data)) {
        array_push(
            $arryflight,
            new chuyenbay(
                $row['macb'],
                $row['ngaydi'],
                $row['giodi'],
                $row['ngayden'],
                $row['gioden'],
                $row['sanbaydi'],
                $row['ma_sanbaydi'],
                $row['sanbayden'],
                $row['ma_sanbayden'],
                $row['duration'],
                $row['status']
            )
        );
    }

    echo json_encode($arryflight);
} else {
    echo json_encode("Error");
}
class chuyenbay
{
    function __construct($macb, $ngaydi, $giodi, $ngayden, $gioden, $sanbaydi, $ma_sanbaydi, $sanbayden, $ma_sanbayden, $duration, $status)
    {
        $this->macb = $macb;
        $this->ngaydi = $ngaydi;
        $this->giodi = $giodi;
        $this->ngayden = $ngayden;
        $this->gioden = $gioden;
        $this->sanbaydi = $sanbaydi;
        $this->ma_sanbaydi = $ma_sanbaydi;
        $this->sanbayden = $sanbayden;
        $this->ma_sanbayden = $ma_sanbayden;
        $this->duration = $duration;
        $this->status = $status;
    }
}

?>