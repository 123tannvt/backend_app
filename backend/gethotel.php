<?php
include "../config/db_connect.php";
$query = "SELECT * FROM hotel";

$data = mysqli_query($conn, $query);
$arrayhotel = array();

$url = 'http://localhost/admin/assets/img/';
$count = mysqli_num_rows($data);
if ($count >= 1) {
    while ($row = mysqli_fetch_array($data)) {

        while ($row = mysqli_fetch_assoc($data)) {
            $price = $row['price'];
            $conver_price = number_format($price, 0, '.', '.');
            array_push($arrayhotel, new hotel(
                $row['id_hotel'],
                $row['name_hotel'],
                $url.$row['img'],
                $row['location'],
                $conver_price,
                $row['rating'],
                $row['detail']
            )
            );
        }
        echo json_encode($arrayhotel);
    }
}
else {
    echo json_encode("Error");
}

class hotel
{
    function __construct( $id_hotel, $name_hotel, $img,$location,$price,$rating,$detail)
    {
        $this->id_hotel = $id_hotel;
        $this->name_hotel = $name_hotel;
        $this->img = $img;
        $this->location = $location;
        $this->price = $price;
        $this->rating = $rating;
        $this->detail = $detail;

    }
}
?>