<?php
include "../config/db_connect.php";
// $masb = $_POST['masb_di'];
$mangsanbay = array();
$query = "SELECT * FROM sanbay WHERE status=1";
$data = mysqli_query($conn, $query);
while ($row = mysqli_fetch_assoc($data)) {
    array_push($mangsanbay, new sanbay(
        $row['masb'],
        $row['tensb'],
        $row['diachi'],
        $row['status']
    ));
}

echo json_encode($mangsanbay);
class sanbay{
    function __construct($masb, $tensb,$diachi,$status){
        $this->masb = $masb;
        $this->tensb = $tensb;
        $this->diachi = $diachi;
        $this->status = $status;
    }
}

?>