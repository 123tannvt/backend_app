<?php include './config/db_connect.php' ?>

<style>
	td p {
		margin:unset;
	}
	td img {
	    width: 8vw;
	    height: 12vh;
	}
	td{
		vertical-align: middle !important;
	}
</style>


<div class="container-fluid">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
				<large class="card-title">
					<b>Booked Flights List</b>
				</large>

			</div>
			<div class="card-body">
				<table class="table table-bordered" id="flight-list">
					<colgroup>
						<col width="10%">
						<col width="30%">
						<col width="50%">
					</colgroup>
					<thead>
						<tr>
							<th class="text-center">#</th>
							<th class="text-center">Information</th>
							<th class="text-center">Flight Info</th>

						</tr>
					</thead>
					<tbody>
						<?php
							$airport = $conn->query("SELECT * FROM sanbay ");
							while($row = $airport->fetch_assoc()){
								$aname[$row['masb']] = ucwords($row['tensb'].', '.$row['diachi']);
							}
							$i=1;
							$qry = $conn->query("SELECT b.*,f.*,a.tenmb,a.logo,b.madv as bid FROM  datve b inner join chuyenbay f on f.macb = b.macb inner join maybay a on f.mamb = a.mamb  order by b.madv desc");
							while($row = $qry->fetch_assoc()):

						 ?>
						 <tr>

						 	<td><?php echo $i++ ?></td>
						 	<td>
						 		<p>Name :<b><?php echo $row['tenkh'] ?></b></p>
						 		<p><small>Contact # :<b><?php echo $row['dienthoai'] ?></small></b></p>
						 		<p><small>Address :<b><?php echo $row['diachi'] ?></small></b></p>
						 	</td>
						 	<td>
						 		<div class="row">
						 		<div class="col-sm-4">
						 			<img src="http://localhost:8080/backend_app/assets/img/<?php echo $row['logo'] ?>" alt="" class="btn-rounder badge-pill">
						 		</div>
						 		<div class="col-sm-6">
						 		<p>Airline :<b><?php echo $row['tenmb'] ?></b></p>
						 		<p><small>Plane :<b><?php echo $row['plane_no'] ?></small></b></p>
						 		<!-- <p><small>Airline :<b><?php echo $row['tenmb'] ?></small></b></p> -->
						 		<p><small>Location :<b><?php echo $aname[$row['sanbaydi_id']].' - '.$aname[$row['sanbayden_id']] ?></small></b></p>
						 		<p><small>Departure :<b><?php echo date('M d,Y h:i A',strtotime($row['giodi'])) ?></small></b></p>
						 		<p><small>Arrival :<b><?php echo date('M d,Y h:i A',strtotime($row['gioden'])) ?></small></b></p>
						 		</div>
						 		</div>
						 	</td>


						 </tr>

						<?php endwhile; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>



<script>
	$('#flight-list').dataTable()
	$('#new_booked').click(function(){
		uni_modal("New Flight","manage_booked.php",'mid-large')
	})
	$('.edit_booked').click(function(){
		uni_modal("Edit Information","manage_booked.php?id="+$(this).attr('data-id'),'mid-large')
	})
// 	$('.delete_booked').click(function(){
// 		_conf("Are you sure to delete this data?","delete_booked",[$(this).attr('data-id')])
// 	})
// function delete_booked($id){
// 		start_load()
// 		$.ajax({
// 			url:'./lib/ajax.php?action=delete_flight',
// 			method:'POST',
// 			data:{id:$id},
// 			success:function(resp){
// 				if(resp==1){
// 					alert_toast("Flight successfully deleted",'success')
// 					setTimeout(function(){
// 						location.reload()
// 					},1500)

// 				}
// 			}
// 		})
// 	}
</script>