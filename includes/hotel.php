<?php include('./config/db_connect.php');?>
<style>

	td{
		vertical-align: middle !important;
	}
	td p{
		margin: unset
	}
	img{
		max-width:100px;
		max-height: :150px;
	}
</style>

<div class="container-fluid">

	<div class="col-lg-12">
		<div class="row">
			<!-- FORM Panel -->
			<div class="col-md-4">
			<form action="" id="manage-hotel">
				<div class="card">
					<div class="card-header">
						  Hotel Form
				  	</div>
					<div class="card-body">
							<input type="hidden" name="id">
							<div class="form-group">
								<label class="control-label">Hotel</label>
								<textarea name="hotel" id="" cols="10" rows="2" class="form-control"></textarea>
							</div>
              <div class="form-group">
								<label class="control-label">Location</label>
								<textarea name="location" id="" cols="30" rows="2" class="form-control"></textarea>
							</div>
              <div class="form-group">
								<label class="control-label">Price</label>
								<textarea name="price" id="" cols="10" rows="2" class="form-control"></textarea>
							</div>
              <div class="form-group">
								<label class="control-label">Rating</label>
								<textarea name="rating" id="" cols="10" rows="2" class="form-control"></textarea>
							</div>
							<div class="form-group">
								<label class="control-label">Detail</label>
								<textarea name="detail" id="" cols="30" rows="2" class="form-control"></textarea>
							</div>
							<div class="form-group">
								<label for="" class="control-label">Img</label>
								<input type="file" class="form-control" name="img" onchange="displayImg(this,$(this))">
							</div>
							<div class="form-group">
								<img src="" alt="" id="cimg">
							</div>


					</div>

					<div class="card-footer">
						<div class="row">
							<div class="col-md-12">
								<button class="btn btn-sm btn-primary col-sm-3 offset-md-3"> Save</button>
								<button class="btn btn-sm btn-default col-sm-3" type="button" onclick="_reset()"> Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</form>
			</div>
			<!-- FORM Panel -->

			<!-- Table Panel -->
			<div class="col-md-8">
				<div class="card">
					<div class="card-body">
						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th class="text-center">Image</th>
									<th class="text-center">Name</th>
                  <th class="text-center">Location</th>
                  <th class="text-center">Rating</th>
                  <th class="text-center">Price</th>
									<th class="text-center">Detail</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$i = 1;
								$cats = $conn->query("SELECT * FROM hotel order by id_hotel asc");
								while($row=$cats->fetch_assoc()):
								?>
								<tr>
									<td class="text-center"><?php echo $i++ ?></td>
									<td class="text-center">
										<img src="http://localhost:8080/backend_app/upload/img/<?php echo $row['img'] ?>" alt="">
									</td>
									<td class="">
										 <b><?php echo $row['name_hotel'] ?></b>
									</td>
                  <td class="">
										 <b><?php echo $row['location'] ?></b>
									</td>
                  <td class="">
										 <b><?php echo $row['rating'] ?></b>
									</td>
                  <td class="">
										 <b><?php echo $row['price'] ?></b>
									</td>
                  <td class="">
										 <b><?php echo $row['detail'] ?></b>
									</td>
									<td class="text-center">
										<button class="btn btn-sm btn-primary edit_airline" type="button"
                    data-id="<?php echo $row['id_hotel'] ?>"
                     data-hotel="<?php echo $row['name_hotel'] ?>"
                     data-location="<?php echo $row['location']?>"
                     data-price="<?php echo $row['price'] ?>"
                     data-rating="<?php echo $row['rating'] ?>"
                     data-detail="<?php echo $row['detail'] ?>"
                     data-img="<?php echo $row['img'] ?>"
                      >Edit</button>

									</td>
								</tr>
								<?php endwhile; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- Table Panel -->
		</div>
	</div>

</div>

<script>
	function _reset(){
		$('#cimg').attr('src','');
		$('[name="id"]').val('');
		$('#manage-hotel').get(0).reset();
	}

	$('#manage-hotel').submit(function(e){
		e.preventDefault()
		start_load()
		$.ajax({
			url:'ajax.php?action=save_hotel',
			data: new FormData($(this)[0]),
		    cache: false,
		    contentType: false,
		    processData: false,
		    method: 'POST',
		    type: 'POST',
			success:function(resp){
				if(resp==1){
					alert_toast("Data successfully added",'success')
					setTimeout(function(){
						location.reload()
					},1500)

				}
				else if(resp==2){
					alert_toast("Data successfully updated",'success')
					setTimeout(function(){
						location.reload()
					},1500)

				}
			}
		})
	})
  function displayImg(input,_this) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
        	$('#cimg').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
	$('.edit_hotel').click(function(){
		start_load()
		var cat = $('#manage-hotel')
		cat.get(0).reset()
		cat.find("[name='id']").val($(this).attr('data-id'))
		cat.find("[name='hotel']").val($(this).attr('data-hotel'))
		cat.find("[name='location']").val($(this).attr('data-location'))
    cat.find("[name='price']").val($(this).attr('data-price'))
    cat.find("[name='rating']").val($(this).attr('data-rating'))
    cat.find("[name='detail']").val($(this).attr('data-detail'))
		cat.find("#cimg").attr("src","http://localhost:8080/backend_app/upload/img/"+$(this).attr('data-img'))
		end_load()
	})

</script>