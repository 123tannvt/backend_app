<?php include '../config/db_connect.php' ?>
<?php

if(isset($_GET['id'])){
	$qry = $conn->query("SELECT * FROM chuyenbay where macb=".$_GET['id']);
	foreach($qry->fetch_array() as $k => $val){
		$$k = $val;
	}
}

?>
<div class="container-fluid">
	<div class="col-lg-12">
		<form id="manage-flight">
			<input type="hidden" name="id" value="<?php echo isset($_GET['id']) ? $_GET['id'] : '' ?>">
			<div class="row">
				<div class="col-md-8">
					<div class="form-group">
						<label for="" class="control-label">Airline</label>
						<select name="mamb" id="mamb" class="custom-select browser-default select2">
							<option></option>
							<?php
							$airline = $conn->query("SELECT * FROM maybay order by tenmb asc");
							while($row = $airline->fetch_assoc()):
							?>
								<option value="<?php echo $row['mamb'] ?>" <?php echo isset($mamb) && $mamb == $row['mamb'] ? "selected" : '' ?>><?php echo $row['tenmb'] ?></option>
							<?php endwhile; ?>
						</select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8">
					<div class="form-group">
						<label for="">Plane No</label>
						<textarea name="plane_no" id="" cols="30" rows="2" class="form-control"><?php echo isset($plane_no) ? $plane_no : '' ?></textarea>
					</div>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-md-6">
					<div class="">
						<label for="">Departure Location</label>
						<select name="sanbaydi_id" id="sanbaydi_id" class="custom-select browser-default select2">
							<option value=""></option>
						<?php
							$airport = $conn->query("SELECT * FROM sanbay order by tensb asc");
						while($row = $airport->fetch_assoc()):
						?>
							<option value="<?php echo $row['masb'] ?>" <?php echo isset($sanbaydi_id) && $sanbaydi_id == $row['masb'] ? "selected" : '' ?>><?php echo $row['diachi'].", ".$row['tensb'] ?></option>
						<?php endwhile; ?>
						</select>

					</div>
				</div>
				<div class="col-md-6">
					<div class="">
						<label for="">Arrival Location</label>
						<select name="sanbayden_id" id="sanbayden_id" class="custom-select browser-default select2">

							<option value=""></option>

						<?php
							$airport = $conn->query("SELECT * FROM sanbay order by tensb asc");
						while($row = $airport->fetch_assoc()):
						?>
							<option value="<?php echo $row['masb'] ?>" <?php echo isset($sanbayden_id) && $sanbayden_id == $row['masb'] ? "selected" : '' ?>><?php echo $row['diachi'].", ".$row['tensb'] ?></option>
						<?php endwhile; ?>
						</select>

					</div>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-md-6">
					<div class="">
						<label for="">Departure Data/Time</label>
						<input type="text" name="giodi" id="giodi" class="form-control datetimepicker" value="<?php echo isset($giodi) ? date("Y-m-d H:i",strtotime($giodi)) : '' ?>">
					</div>
				</div>
				<div class="col-md-6">
					<div class="">
						<label for="">Arrival Data/Time</label>
						<input type="text" name="gioden" id="gioden" class="form-control datetimepicker" value="<?php echo isset($gioden) ? date("Y-m-d H:i",strtotime($gioden)) : '' ?>">
					</div>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-md-6">
					<div class="">
						<label for="">Duration</label>
						<input type="text" name="duration" id="duration" class="form-control text-right" value="<?php echo isset($duration) ? $duration : '' ?>">
					</div>
					</div>
					<div class="col-md-6">
						<div class="">
							<label for="">Status</label>
							<input type="text" name="status" id="status" class="form-control text-right" value="<?php echo isset($status) ? $status : '' ?>">
						</div>
						</div>
				</div>
			<div class="row form-group">
				<div class="col-md-6">
					<div class="">
						<label for="">Seats</label>
						<input name="seats" id="seats" type="number" step="any" class="form-control text-right" value="<?php echo isset($seats) ? $seats : '' ?>">
					</div>
				</div>
				<div class="col-md-6">
					<div class="">
						<label for="">Price</label>
						<input name="price" id="price" type="number" step="any" class="form-control text-right" value="<?php echo isset($price) ? $price : '' ?>">
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<script>
	$(document).ready(function(){
		$('.select2').each(function(){
		$(this).select2({
		    placeholder:"Please select here",
		    width: "100%"
		  })
	})
	})
	 $('.datetimepicker').datetimepicker({
      format:'Y-m-d H:i',
  })
	 $('#manage-flight').submit(function(e){
	 	e.preventDefault()
	 	start_load()
	 	$.ajax({
	 		url:'ajax.php?action=save_flight',
	 		method:'POST',
	 		data:$(this).serialize(),
	 		success:function(resp){
	 			if(resp == 1){
	 				alert_toast("Flight successfully saved.","success");
	 				setTimeout(function(e){
	 					location.reload()
	 				},1500)
	 			}
	 		}
	 	})
	 })
	 $('.datetimepicker').attr('autocomplete','off')
</script>